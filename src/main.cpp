#include <ESP8266WiFi.h>
#include <ArduinoOTA.h>
#include <FS.h>                       //internal file system
#include <ESP8266WebServer.h>
#include "private.h"

#define MODE 0                        //0->STA 1->AP
#define r_pin 12
#define g_pin 13
#define b_pin 14
#define relay_pin 5
ESP8266WebServer server(80);          //port 80 - browset port for easy display
static int rgb[3];
static bool fade = 1;
static int fade_phase = 0;
static int fade_delay = 100;
void start_OTA(void);
String readFile(String plik);   
void handle_root(); 
void handle_set(); 
void handle_relay();
void handle_fade(); 
void handle_light_off();
void handle_status();
int fade_R(int value);
int fade_G(int value);
int fade_B(int value);

void setup() {

  pinMode(r_pin,OUTPUT);
  pinMode(g_pin,OUTPUT);
  pinMode(b_pin,OUTPUT);
  pinMode(relay_pin, OUTPUT);

  analogWrite(r_pin,1023);
  analogWrite(g_pin,0);
  analogWrite(b_pin,0);
  digitalWrite(relay_pin, HIGH);

  Serial.begin(115200);
  if (!SPIFFS.begin()) ESP.reset();    //open memory acces

  Serial.println("Booting");
#if MODE
  //AP mode
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  
  IPAddress localIp(192,168,0,100);
  IPAddress gateway(192,168,0,100);
  IPAddress subnet(255,255,255,0);
  
  WiFi.softAPConfig(localIp, gateway, subnet);
  WiFi.softAP("LEDy");

#else
  // STA MODE
  IPAddress serverIP(192,168,0,202);
  IPAddress gateway(192,168,0,1);
  IPAddress mask(255,255,255,0);
  IPAddress dns(8,8,8,8);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  WiFi.config(serverIP, gateway, mask, dns);        //const IP
  
  while(WiFi.status() != WL_CONNECTED) {
    Serial.println(".");
    delay(400);
  }
#endif

  start_OTA();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/",HTTP_GET, handle_root);
  server.on("/set",HTTP_POST, handle_set);
  server.on("/relay", handle_relay);
  server.on("/status", handle_status);
  server.on("/fade", handle_fade);
  server.on("/light_off", handle_light_off);
  server.begin();

  analogWrite(r_pin,1023);
  analogWrite(g_pin,0);
  analogWrite(b_pin,500);
}


void loop() {

  ArduinoOTA.handle();
  server.handleClient();
  if (fade){
    analogWrite(r_pin, fade_R(fade_phase));
    analogWrite(g_pin, fade_G(fade_phase));
    analogWrite(b_pin, fade_B(fade_phase));
    fade_phase++;
    delay(fade_delay);
  }
}



void handle_status(){
  String info = 'L'+String(digitalRead(relay_pin));
  info+= 'R'+String(rgb[0]);
  info+= 'G'+String(rgb[1]);
  info+= 'B'+String(rgb[2])+';';
  server.send(200, "text/html", info);
}



void handle_root()
{
  server.send(200, "text/html", readFile("/index.html"));
  
}

void handle_set()
{
  rgb[0] = atoi(server.arg("R").c_str());
  rgb[1] = atoi(server.arg("G").c_str());
  rgb[2] = atoi(server.arg("B").c_str());
  analogWrite(r_pin, 4*rgb[0]);
  analogWrite(g_pin, 4*rgb[1]);
  analogWrite(b_pin, 4*rgb[2]);

  server.send(200, "text/html", readFile("/index.html"));

  fade = 0;
}

void handle_light_off(){

  analogWrite(r_pin, 0);
  analogWrite(g_pin, 0);
  analogWrite(b_pin, 0);
  
  server.send(200, "text/html", readFile("/index.html"));
  fade = 0;
}

void handle_fade(){
  fade = 1;
  fade_delay = atoi(server.arg("delay").c_str());
  fade_delay*=2;
  server.send(200, "text/html", readFile("/index.html"));
}

void handle_relay(){

  digitalWrite(relay_pin, !digitalRead(relay_pin));
  server.send(200, "text/html", readFile("/index.html"));
}

int fade_G(int value){
  int color_G = 0;
  if (value>=60 && value <=180) color_G=1020;
  else if (value>=240 && value <=360) color_G=0;
  else if (value < 60) color_G = 17*value;
  else if (value > 180) color_G = 1020-(17*(value-180));
  return color_G;
}

int fade_R(int value){
  int color_R = 0;
  int buffer = value + 120;
  buffer %= 360;
  color_R = fade_G(buffer);
  return color_R;
}

int fade_B(int value){
  int color_B = 0;
  int buffer = value + 240;
  buffer %= 360;
  color_B = fade_G(buffer);
  return color_B;
}

void start_OTA(){
///OTA//////////////////////
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)   
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";
    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
    
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {

    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
///////////////////////////////////////////////////////////////////////////////////////////////
}

String readFile(String plik)                //from SPIFFS
{
  String strona;
  File file = SPIFFS.open(plik, "r");
  while (file.available())
    strona += (char)file.read();
  file.close();
  return strona;
}
